## EZProxy Groups for Roles

The [EZProxy](https://www.drupal.org/project/ezproxy) module provides Drupal
Users access to databases that may have restricted access. The EZProxy system
can restrict access to Groups that are managed by the EZProxy provider. This
module allows these Groups to be mapped to Drupal User Roles so that when a
Drupal User tries to authenticate to EZProxy the Groups can be sent based on
the Roles the User has assigned to them.

### Project status
The EZProxy module currently has a [patch in the issue queue](https://www.drupal.org/project/ezproxy/issues/2932159)
that will allow third party modules to add Groups to the authentication
process. Until this patch is accepted you will need to apply this patch to the
module.

### drush make
Using [drush](https://www.drush.org/) and a build file for
[drush make](https://docs.drush.org/en/7.x/make/) is an easy way to add EZProxy
and have it patched for you.

Add something like the following to your build file;
```
core = 7.x
api = 2

;; DEFAULTS
defaults[projects][type] = module
defaults[projects][subdir] = contrib

;; PROJECTS
projects[ezproxy][version] = 2
projects[ezproxy][patch][] = "https://www.drupal.org/files/issues/2019-04-17/ezproxy-compatibility-with-latest-version-2932159-3.patch"
```

### Usage
Once enabled you will want to apply the access ezproxy content permission to
the Roles you want to use. Once done these Roles will become available on the
Administration » Configuration » EZProxy » EZProxy Groups page. Add the
Groups set up with your EZProxy provider to the Roles you want to be able to
access specific resources. Then give your Users the appropriate Roles.

### Reporting issues
Please use the Issue Summary Template and provide as much information as
possible to replicate the issue. If you can't describe it, we can't replicate
it. If we can't replicate it, is it really an issue to report?
