<?php

/**
 * @file
 * Administration forms for EZProxy Groups for Roles.
 */

/**
 * Roles form to allow user defined Groups to be added.
 */
function ezproxy_gfr_form($form, &$form_state) {
  $roles = user_roles(FALSE, 'access ezproxy content');
  $vals = variable_get('ezproxy_gfr', array());

  $g = array();
  ezproxy_gfr_ezproxy_add_groups_alter($g);

  $form['ezproxy_gfr'] = array(
    '#title' => t('Groups for Roles'),
    '#type' => 'fieldset',
    '#description' => t('Enter the EZProxy Group(s) that should be returned for each role a user has.  One per line. Groups are case sensitive. For a Role to appear here the "access ezproxy content" permission needs to be applied to it.'),
    '#tree' => TRUE,
  );
  foreach ($roles as $rid => $role) {
    $form['ezproxy_gfr'][$rid] = array(
      '#title' => t('Groups for %role role:', array('%role' => $role)),
      '#description' => t('List one EZProxy Group per line.'),
      '#type' => 'textarea',
      '#default_value' => (!empty($vals[$rid])) ? $vals[$rid] : '',
    );
  }

  return system_settings_form($form);
}
